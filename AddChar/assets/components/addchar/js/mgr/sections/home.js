addchar.page.Home = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        components: [{
            xtype: 'addchar-panel-home',
            renderTo: 'addchar-panel-home-div'
        }]
    });
    addchar.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(addchar.page.Home, MODx.Component);
Ext.reg('addchar-page-home', addchar.page.Home);