addchar.combo.charType = function (config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.SimpleStore({
            id: 0,
            fields: ['name', 'type'],
            data: [
                ['Число', 'integer'],
                ['С плавающей точкой', 'float'],
                ['Строка', 'string'],
                ['JSON', 'json']
            ]
        }),
        emptyText: _('addchar_char_create_choosetype'),
        mode: 'local',
        displayField: 'name',
        valueField: 'type',
        hiddenName: 'type'
    });
    addchar.combo.charType.superclass.constructor.call(this, config);
};
Ext.extend(addchar.combo.charType, MODx.combo.ComboBox);
Ext.reg('char-combo-types', addchar.combo.charType);

addchar.window.CreateChar = function (config) {
    config = config || {};
    if (!config.id) {
        config.id = 'addchar-char-window-create';
    }
    Ext.applyIf(config, {
        title: _('addchar_char_create_title'),
        width: 550,
        autoHeight: true,
        url: addchar.config.connector_url,
        action: 'mgr/char/create',
        fields: this.getFields(config),
        keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    addchar.window.CreateChar.superclass.constructor.call(this, config);
};
Ext.extend(addchar.window.CreateChar, MODx.Window, {

    getFields: function (config) {
        return [{
            xtype: 'textfield',
            fieldLabel: _('addchar_char_create_key'),
            name: 'key',
            id: config.id + '-key',
            anchor: '99%',
            allowBlank: false
        }, {
            xtype: 'textfield',
            fieldLabel: _('addchar_char_create_name'),
            name: 'title',
            id: config.id + '-name',
            anchor: '99%',
            allowBlank: false
        }, {
            xtype: 'char-combo-types',
            fieldLabel: _('addchar_char_create_type'),
            name: 'type',
            id: config.id + '-type',
            anchor: '99%',
            allowBlank: false
        }];
    },

    loadDropZones: function () {
    }

});
Ext.reg('addchar-char-window-create', addchar.window.CreateChar);


addchar.window.UpdateChar = function (config) {
    config = config || {};
    if (!config.id) {
        config.id = 'addchar-char-window-update';
    }
    Ext.applyIf(config, {
        title: _('addchar_item_update'),
        width: 550,
        autoHeight: true,
        url: addchar.config.connector_url,
        action: 'mgr/char/update',
        fields: this.getFields(config),
        keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    addchar.window.UpdateChar.superclass.constructor.call(this, config);
};
Ext.extend(addchar.window.UpdateChar, MODx.Window, {

    getFields: function (config) {
        return [{
            xtype: 'textfield',
            fieldLabel: _('addchar_char_create_key'),
            readOnly: true,
            cls: 'x-item-disabled',
            name: 'key',
            id: config.id + '-key',
            anchor: '99%',
            allowBlank: false
        }, {
            xtype: 'textfield',
            fieldLabel: _('addchar_char_create_name'),
            name: 'title',
            id: config.id + '-name',
            anchor: '99%',
            allowBlank: false
        }, {
            xtype: 'char-combo-types',
            fieldLabel: _('addchar_char_create_type'),
            name: 'type',
            id: config.id + '-type',
            anchor: '99%',
            allowBlank: false
        }];
    },

    loadDropZones: function () {
    }

});
Ext.reg('addchar-char-window-update', addchar.window.UpdateChar);