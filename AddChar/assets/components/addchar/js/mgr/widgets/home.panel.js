addchar.panel.Home = function (config) {
    config = config || {};
    Ext.apply(config, {
        baseCls: 'modx-formpanel',
        layout: 'anchor',

        // stateful: true,
        // stateId: 'addchar-panel-home',
        // stateEvents: ['tabchange'],
        // getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},

        hideMode: 'offsets',
        items: [{
            html: '<h2>'+_('addchar_list_chars_title')+'</h2>',
            cls: '',
            style: {margin: '15px 5px'}
        }, {
            xtype: 'modx-tabs',
            defaults: {border: false, autoHeight: true},
            border: true,
            hideMode: 'offsets',
            items: [{
                title: _('addchar_list_chars_tab_title'),
                layout: 'anchor',
                items: [{
                    html: _('addchar_list_chars_tab_desc') + '<br>' +_('addchar_list_chars_tab_msg'),
                    cls: 'panel-desc'
                }, {
                    xtype: 'addchar-grid-chars',
                    cls: 'main-wrapper'
                }]
            }]
        }]
    });
    addchar.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(addchar.panel.Home, MODx.Panel);
Ext.reg('addchar-panel-home', addchar.panel.Home);