miniShop2.page.UpdateCategoryAdditional = function (config) {
    config = config || {record: {}};
    config.record = config.record || {};
    Ext.applyIf(config, {
        panelXType: 'minishop2-panel-category-update-additional',
        mode: 'update',
        actions: {
            new: 'resource/create',
            edit: 'resource/update',
            preview: 'resource/preview',
        }
    });
    miniShop2.page.UpdateCategoryAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.page.UpdateCategoryAdditional, miniShop2.page.UpdateCategory);
Ext.reg('minishop2-page-category-update-additional', miniShop2.page.UpdateCategoryAdditional);


miniShop2.panel.UpdateCategoryAdditional = function (config) {
    config = config || {};
    miniShop2.panel.UpdateCategoryAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.panel.UpdateCategoryAdditional, miniShop2.panel.UpdateCategory, {

    getProducts: function (config) {
        return {
            title: _('ms2_tab_products'),
            id: 'modx-minishop2-products',
            layout: 'anchor',
            items: [{
                xtype: 'minishop2-grid-products-additional',
                resource: config.resource,
                border: false,
                listeners: {

                },
            }]
        };
    },
});
Ext.reg('minishop2-panel-category-update-additional', miniShop2.panel.UpdateCategoryAdditional);