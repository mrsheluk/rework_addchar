var addchar = function (config) {
    config = config || {};
    addchar.superclass.constructor.call(this, config);
};
Ext.extend(addchar, Ext.Component, {
    page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('addchar', addchar);

addchar = new addchar();