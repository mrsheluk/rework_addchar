miniShop2.panel.GalleryAdditional = function (config) {
    config = config || {};

    Ext.apply(config, {
        border: false,
        id: 'minishop2-gallery-page',
        baseCls: 'x-panel',
        items: [{
            border: false,
            style: {padding: '10px 5px'},
            xtype: 'minishop2-gallery-page-toolbar',
            id: 'minishop2-gallery-page-toolbar',
            record: config.record,
        }, {
            border: false,
            style: {padding: '5px'},
            layout: 'anchor',
            items: [{
                border: false,
                xtype: 'minishop2-gallery-images-panel-additional',
                id: 'minishop2-gallery-images-panel-additional',
                cls: 'modx-pb-view-ct',
                product_id: config.record.id,
                pageSize: config.pageSize
            }]
        }]
    });

    miniShop2.panel.GalleryAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.panel.GalleryAdditional, miniShop2.panel.Gallery, {

    _initUploader: function () {
        console.log('here');
        var params = {
            action: 'mgr/gallery/upload',
            id: this.record.id,
            source: this.record.source,
            ctx: 'mgr',
            HTTP_MODAUTH: MODx.siteId
        };

        this.uploader = new plupload.Uploader({
            url: miniShop2.config.addchar_connector_url + '?' + Ext.urlEncode(params),
            browse_button: 'minishop2-resource-upload-btn',
            container: this.id,
            drop_element: this.id,
            multipart: true,
            max_file_size: miniShop2.config.media_source.maxUploadSize || 10485760,
            filters: [{
                title: "Image files",
                extensions: miniShop2.config.media_source.allowedFileTypes || 'jpg,jpeg,png,gif'
            }],
            resize: {
                width: miniShop2.config.media_source.maxUploadWidth || 1920,
                height: miniShop2.config.media_source.maxUploadHeight || 1080
            }
        });

        var uploaderEvents = ['FilesAdded', 'FileUploaded', 'QueueChanged', /*'UploadFile',*/ 'UploadProgress', 'UploadComplete'];
        Ext.each(uploaderEvents, function (v) {
            var fn = 'on' + v;
            this.uploader.bind(v, this[fn], this);
        }, this);
        this.uploader.init();
    }
});
Ext.reg('minishop2-gallery-page-additional', miniShop2.panel.GalleryAdditional);