<?php
/** @var xPDOTransport $transport */
/** @var array $options */
/** @var modX $modx */
if ($transport->xpdo) {
    $modx =& $transport->xpdo;

    $dev = MODX_BASE_PATH . 'Extras/addchar/';
    /** @var xPDOCacheManager $cache */
    $cache = $modx->getCacheManager();
    if (file_exists($dev) && $cache) {
        if (!is_link($dev . 'assets/components/addchar')) {
            $cache->deleteTree(
                $dev . 'assets/components/addchar/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink(MODX_ASSETS_PATH . 'components/addchar/', $dev . 'assets/components/addchar');
        }
        if (!is_link($dev . 'core/components/addchar')) {
            $cache->deleteTree(
                $dev . 'core/components/addchar/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink(MODX_CORE_PATH . 'components/addchar/', $dev . 'core/components/addchar');
        }
    }
}

return true;