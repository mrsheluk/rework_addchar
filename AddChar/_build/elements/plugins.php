<?php

return [
    'ClassMapUpdater' => [
        'file' => 'class_map_updater',
        'description' => 'Плагин скрывает создание msCategory и msProduct',
        'events' => [
            'OnMODXInit' => [
                'priority' => 0
            ]
        ]
    ],
];