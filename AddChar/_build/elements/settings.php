<?php

return [
    'mgr_tree_icon_accategory' => [
        'key' => 'mgr_tree_icon_accategory',
        'xtype' => 'textfield',
        'value' => 'icon-inbox',
        'area' => 'addchar_main',
    ],
    'mgr_tree_icon_acproduct' => [
        'key' => 'mgr_tree_icon_acproduct',
        'xtype' => 'textfield',
        'value' => 'icon-money',
        'area' => 'addchar_main',
    ],
    /*
    'cart_handler_class' => [
        'key' => 'ms2_cart_handler_class',
        'xtype' => 'textfield',
        'value' => 'acCartHandler',
        'area' => 'ms2_cart',
        'namespace' => 'minishop2',
    ],
    */
];