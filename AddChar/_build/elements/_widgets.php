<?php

return [
    'addchar' => [
        'description' => '',
        'type' => 'file',
        'content' => '',
        'namespace' => 'addchar',
        'lexicon' => 'addchar:dashboards',
        'size' => 'half',
    ],
];