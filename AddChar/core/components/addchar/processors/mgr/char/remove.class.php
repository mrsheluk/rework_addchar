<?php

class acCharRemoveProcessor extends modObjectProcessor {

    public $classKey = 'acChar';
    public $languageTopics = ['default', 'addchar:default'];

    public function process()
    {
        if (!$this->checkPermissions()) {
            return $this->failure($this->modx->lexicon('access_denied'));
        }
        $keys = $this->modx->fromJSON($this->getProperty('keys'));
        if (empty($keys)) {
            return $this->failure($this->modx->lexicon('addchar_char_err_nf'));
        }
        foreach ($keys as $key) {
            /** @var modExtraItem $object */
            if (!$object = $this->modx->getObject($this->classKey, $key)) {
                return $this->failure($this->modx->lexicon('addchar_char_err_nf'));
            }
            $object->remove();
        }
        return $this->success();
    }

}

return 'acCharRemoveProcessor';