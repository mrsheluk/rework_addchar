<?php

require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/category/update.class.php';

class acCategoryUpdateProcessor extends msCategoryUpdateProcessor
{
    public $classKey = 'acCategory';
}