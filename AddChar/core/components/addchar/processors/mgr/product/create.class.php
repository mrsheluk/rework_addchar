<?php

require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/product/create.class.php';

class acProductCreateProcessor extends msProductCreateProcessor
{
    public $classKey = 'acProduct';

}

return 'acProductCreateProcessor';