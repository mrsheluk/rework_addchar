<?php

require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/product/update.class.php';

class acProductUpdateProcessor extends msProductUpdateProcessor
{
    public $classKey = 'acProduct';

}

return 'acProductUpdateProcessor';