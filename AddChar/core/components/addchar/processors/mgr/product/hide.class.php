<?php

class acProductHideInTreeProcessor extends modObjectUpdateProcessor
{
    public $classKey = 'acProduct';


    /**
     * @return bool
     */
    public function beforeSet()
    {
        $this->properties = array(
            'show_in_tree' => false,
        );

        return true;
    }

}

return 'acProductHideInTreeProcessor';