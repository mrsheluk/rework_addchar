<?php

if ($modx->event->name !== 'OnMODXInit' || $modx->context->key != 'mgr')
    return;

/** @var array $ExcludeClasses Список исключаемых плагином классов */
$ExcludeClasses = [
    'modResource' => [ // Родительский класс
        'msProduct', 'msCategory' // Классы, которые наследуют родительский класс и должны быть исключены
    ]
];

// Пытаемся создать отражение для объекта $modx
try {
    $modxReflection = new ReflectionClass($modx);
} catch (ReflectionException $refEx) {
    // TODO: Catch reflection exception
    return;
}

// Если вдруг (мало ли) у объекта modx нет свойства classMap - завершаем работу плагина
if (!$modxReflection->hasProperty('classMap'))
    return;

// Создаём отражение свойства classMap объекта $modx
$classMapReflection = $modxReflection->getProperty('classMap');
if(!$classMapReflection->isPublic())
    $classMapReflection->setAccessible(true);

// Получаем значение свойства classMap
$classMapValue = $classMapReflection->getValue($modx);

// Обходим все исключаемые классы
foreach ($ExcludeClasses as $parentClass => $derivativeClasses) {
    if (!isset($classMapValue[$parentClass]))
        continue;

    foreach ($derivativeClasses as $derivativeClass) {
        if (($index = array_search($derivativeClass, $classMapValue[$parentClass])) !== false) {
            array_splice($classMapValue[$parentClass], $index, 1);
        }
    }
}

// Устанавливаем новое значение для свойства classMap объекта $modx
$classMapReflection->setValue($modx, $classMapValue);