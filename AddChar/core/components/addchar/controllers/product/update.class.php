<?php

require_once MODX_CORE_PATH . 'components/minishop2/controllers/product/update.class.php';

class acProductUpdateManagerController extends msProductUpdateManagerController {

    /**
     * Returns language topics
     * @return array
     */
    public function getLanguageTopics() {
        return array_merge(['addchar:default', 'addchar:product'], parent::getLanguageTopics());
    }

    public function loadCustomCssJs() {
        $mgrUrl = $this->modx->getOption('manager_url', null, MODX_MANAGER_URL);
        $assetsUrl = $this->miniShop2->config['assetsUrl'];
        $this->addchar = $this->modx->getService('addChar', 'addchar', MODX_CORE_PATH . 'components/addchar/model/');
        $jsPath = $this->addchar->config['jsUrl'];

        $this->addCss($assetsUrl . 'css/mgr/main.css');
        $this->addJavascript($mgrUrl . 'assets/modext/util/datetime.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/element/modx.panel.tv.renders.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/resource/modx.grid.resource.security.local.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/resource/modx.panel.resource.tv.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/resource/modx.panel.resource.js');
        $this->addJavascript($mgrUrl . 'assets/modext/sections/resource/update.js');
        $this->addJavascript($assetsUrl . 'js/mgr/minishop2.js');
        $this->addJavascript($assetsUrl . 'js/mgr/misc/ms2.combo.js');
        $this->addJavascript($assetsUrl . 'js/mgr/misc/ms2.utils.js');
        $this->addJavascript($assetsUrl . 'js/mgr/misc/default.grid.js');
        $this->addJavascript($assetsUrl . 'js/mgr/misc/default.window.js');
        $this->addJavascript($jsPath . 'mgr/misc/addchar_product_char.js');
        $this->addLastJavascript($assetsUrl . 'js/mgr/product/category.tree.js');
        $this->addLastJavascript($assetsUrl . 'js/mgr/product/links.grid.js');
        $this->addLastJavascript($assetsUrl . 'js/mgr/product/links.window.js');
        $this->addLastJavascript($assetsUrl . 'js/mgr/product/product.common.js');
        $this->addLastJavascript($jsPath . 'mgr/product/product.js');
        $this->addLastJavascript($assetsUrl . 'js/mgr/product/update.js');
        $this->addLastJavascript($jsPath . 'mgr/product/update.js');

        $show_gallery = $this->modx->getOption('ms2_product_tab_gallery', null, true) &&
            !($this->modx->getOption('ms2gallery_sync_ms2', null, false, true));
        if ($show_gallery) {
            $this->addLastJavascript($assetsUrl . 'js/mgr/misc/plupload/plupload.full.min.js');
            $this->addLastJavascript($assetsUrl . 'js/mgr/misc/ext.ddview.js');
            $this->addLastJavascript($assetsUrl . 'js/mgr/product/gallery/gallery.panel.js');
            $this->addLastJavascript($jsPath . 'mgr/gallery/gallery.panel.js');
            $this->addLastJavascript($assetsUrl . 'js/mgr/product/gallery/gallery.toolbar.js');
            $this->addLastJavascript($assetsUrl . 'js/mgr/product/gallery/gallery.view.js');
            $this->addLastJavascript($jsPath . 'mgr/gallery/gallery.image.js');
            $this->addLastJavascript($assetsUrl . 'js/mgr/product/gallery/gallery.window.js');
        }

        // Customizable product fields feature
        $product_fields = array_merge($this->resource->getAllFieldsNames(), array('syncsite'));
        $product_data_fields = $this->resource->getDataFieldsNames();

        if (!$product_main_fields = $this->modx->getOption('ms2_product_main_fields')) {
            $product_main_fields = 'pagetitle,longtitle,introtext,content,publishedon,pub_date,unpub_date,template,
                parent,alias,menutitle,searchable,cacheable,richtext,uri_override,uri,hidemenu,show_in_tree';
        }
        $product_main_fields = array_map('trim', explode(',', $product_main_fields));
        $product_main_fields = array_values(array_intersect($product_main_fields, $product_fields));

        if (!$product_extra_fields = $this->modx->getOption('ms2_product_extra_fields')) {
            $product_extra_fields = 'article,price,old_price,weight,color,remains,reserved,vendor,made_in,tags';
        }
        $product_extra_fields = array_map('trim', explode(',', $product_extra_fields));
        $product_extra_fields = array_values(array_intersect($product_extra_fields, $product_fields));
        $product_option_fields = $this->resource->loadData()->getOptionFields();

        // addChars
        $parent = $this->modx->resource->id;
        $old_crutch = [44 => 143485, 114912 => 143486, 114805 => 114803];
        parent_for_tv:
        if ($parent != 50 && $parent != 0) {
            $q = $this->modx->newQuery('modResource', $parent);$q->select('parent');
            $parent = $this->modx->getValue($q->prepare());
            if ($old_crutch[$parent]) $parent = $old_crutch[$parent];
            $q = $this->modx->newQuery('modTemplateVarResource', ['tmplvarid' => 150, 'contentid' => $parent]);$q->select('value');
            if (!$tmp = $this->modx->getValue($q->prepare()) or count(json_decode($tmp, 1)) <= 0) goto parent_for_tv;
            $tmp = array_column(json_decode($tmp, 1), 'title', 'field');
            $product_additional_fields = $this->resource->proccessChars();
            $length = count($product_additional_fields);
            for ($i = 0; $i < $length; $i++) {
                if ($tmp[$product_additional_fields[$i]['key']]) {
                    $product_additional_fields[$i]['title'] = $tmp[$product_additional_fields[$i]['key']];
                } else {
                    unset($product_additional_fields[$i]);
                }
            }
            $product_additional_fields = array_values($product_additional_fields);
        } else {
            $product_additional_fields = [];
        }

        $this->prepareFields();
        //---

        $show_comments = class_exists('Ticket') && $this->modx->getOption('ms2_product_show_comments');
        if ($show_comments) {
            $this->loadTickets();
        }
        $neighborhood = array();
        if ($this->resource instanceof acProduct) {
            $neighborhood = $this->resource->getNeighborhood();
        }

        $config = array(
            'assets_url' => $this->miniShop2->config['assetsUrl'],
            'connector_url' => $this->miniShop2->config['connectorUrl'],
            'show_comments' => $show_comments,
            'show_gallery' => $show_gallery,
            'show_extra' => (bool)$this->modx->getOption('ms2_product_tab_extra', null, true),
            'show_options' => (bool)$this->modx->getOption('ms2_product_tab_options', null, true),
            'show_links' => (bool)$this->modx->getOption('ms2_product_tab_links', null, true),
            'show_categories' => (bool)$this->modx->getOption('ms2_product_tab_categories', null, true),
            'default_thumb' => $this->miniShop2->config['defaultThumb'],
            'main_fields' => $product_main_fields,
            'extra_fields' => $product_extra_fields,
            'option_fields' => $product_option_fields,
            'data_fields' => $product_data_fields,
            'additional_fields' => array(),
            'media_source' => $this->getSourceProperties(),
            'addchar_additional' => $product_additional_fields,
            'addchar_connector_url' => $this->addchar->config['connectorUrl'],
        );

        $ready = array(
            'xtype' => 'minishop2-page-product-update-additional',
            'resource' => $this->resource->get('id'),
            'record' => $this->resourceArray,
            'publish_document' => $this->canPublish,
            'preview_url' => $this->previewUrl,
            'locked' => $this->locked,
            'lockedText' => $this->lockedText,
            'canSave' => $this->canSave,
            'canEdit' => $this->canEdit,
            'canCreate' => $this->canCreate,
            'canDuplicate' => $this->canDuplicate,
            'canDelete' => $this->canDelete,
            'canPublish' => $this->canPublish,
            'show_tvs' => !empty($this->tvCounts),
            'next_page' => !empty($neighborhood['right'][0])
                ? $neighborhood['right'][0]
                : 0,
            'prev_page' => !empty($neighborhood['left'][0])
                ? $neighborhood['left'][0]
                : 0,
            'up_page' => $this->resource->parent,
            'mode' => 'update',
        );

        $this->addHtml('
        <script type="text/javascript">
        // <![CDATA[
        MODx.config.publish_document = "' . $this->canPublish . '";
        MODx.onDocFormRender = "' . $this->onDocFormRender . '";
        MODx.ctx = "' . $this->ctx . '";
        miniShop2.config = ' . json_encode($config) . ';
        Ext.onReady(function() {
            MODx.load(' . json_encode($ready) . ');
        });
        // ]]>
        </script>');

        // load RTE
        $this->loadRichTextEditor();
        $this->modx->invokeEvent('msOnManagerCustomCssJs', array('controller' => &$this, 'page' => 'product_update'));
        $this->loadPlugins();
    }

    /**
     * Additional preparation of the resource fields
     */
    function prepareFields()
    {
        $data = array_keys(array_merge($this->modx->getFieldMeta('msProductData'), $this->resource->loadChars()));
        foreach ($this->resourceArray as $k => $v) {
            if (is_array($v) && in_array($k, $data)) {
                $tmp = $this->resourceArray[$k];
                $this->resourceArray[$k] = array();
                foreach ($tmp as $v2) {
                    if (!empty($v2)) {
                        $this->resourceArray[$k][] = array('value' => $v2);
                    }
                }
            }
        }

        if (empty($this->resourceArray['vendor'])) {
            $this->resourceArray['vendor'] = '';
        }
    }
}