<?php

$_lang['product_characteristics_tab'] = 'Характеристики товара';
$_lang['ac_product_create'] = 'Создать товар с доп. хар-ми';
$_lang['ac_product_create_here'] = 'Создать товар с доп. хар-ми';
$_lang['ac_product_name'] = 'Товар с доп. хар-ми';
$_lang['ac_product_tab_name'] = 'Доп. хар-ки';

$_lang['ac_product_char_custom_color'] = 'Цвет';
$_lang['ac_product_char_aplication'] = 'Применение';
$_lang['ac_product_char_count_lamps'] = 'Кол-во ламп';
$_lang['ac_product_char_mount_type'] = 'Способ установки';
$_lang['ac_product_char_product_type'] = 'Тип';

$_lang['ac_product_char_custom_color_help'] = '';
$_lang['ac_product_char_aplication_help'] = '';
$_lang['ac_product_char_count_lamps_help'] = '';
$_lang['ac_product_char_mount_type_help'] = '';
$_lang['ac_product_char_product_type_help'] = '';