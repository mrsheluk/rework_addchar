<?php
include_once 'setting.inc.php';

$_lang['addchar'] = 'AddChar';
$_lang['addchar_menu_desc'] = 'Менеджер дополнительных характеристик.';

$_lang['addchar_list_chars_title'] = 'Дополнительные характеристики товаров';
$_lang['addchar_list_chars_tab_title'] = 'Список характеристик';
$_lang['addchar_list_chars_tab_desc'] = 'Менеджер дополнительных характеристик для товаров.';
$_lang['addchar_list_chars_tab_msg'] = 'Вы можете выделять сразу несколько характеристик при помощи Shift или Ctrl.';

$_lang['addchar_common_edit'] = 'Редактировать';
$_lang['addchar_common_delete'] = 'Удалить';

$_lang['addchar_grid_title_key'] = 'Ключ';
$_lang['addchar_grid_title_name'] = 'Имя';
$_lang['addchar_grid_title_type'] = 'Тип';
$_lang['addchar_grid_title_actions'] = 'Действия';
$_lang['addchar_grid_button_create'] = 'Создать';
$_lang['addchar_grid_char_delete'] = 'Удалить характеристику';
$_lang['addchar_grid_chars_delete'] = 'Удалить характеристики';
$_lang['addchar_grid_char_delete_confirm'] = 'Вы уверены что хотите удалить характеристику?';
$_lang['addchar_grid_chars_delete_confirm'] = 'Вы уверены что хотите удалить характеристики?';

$_lang['addchar_char_create_title'] = 'Создать характеристику';
$_lang['addchar_char_create_key'] = 'Ключ';
$_lang['addchar_char_create_name'] = 'Имя';
$_lang['addchar_char_create_type'] = 'Тип';
$_lang['addchar_char_create_choosetype'] = 'выберите тип';

$_lang['addchar_char_err_nf'] = 'Харакеристика не найдена.';

$_lang['addchar_category_create'] = 'Категорию с доп. хар-ми';
$_lang['addchar_category_type'] = 'Категория с доп. хар-ми';