<?php

/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/category/create.class.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/category/update.class.php';

class acCategory extends modResource {

    public $showInContextMenu = true;

    function __construct(xPDO & $xpdo)
    {
        parent:: __construct($xpdo);
        $this->set('class_key', 'acCategory');
    }


    public static function getControllerPath(xPDO &$modx)
    {
        return $modx->getOption('addchar.core_path', null,
                $modx->getOption('core_path') . 'components/addchar/') . 'controllers/category/';
    }


    public function getContextMenuText()
    {
        $this->xpdo->lexicon->load('addchar:default');

        return array(
            'text_create' => $this->xpdo->lexicon('addchar_category_create'),
            'text_create_here' => $this->xpdo->lexicon('addchar_category_create'),
        );
    }


    public function getResourceTypeName()
    {
        $this->xpdo->lexicon->load('addchar:default');

        return $this->xpdo->lexicon('addchar_category_type');
    }


    public function save($cacheFlag = null)
    {
        if (!$this->isNew() && parent::get('class_key') != 'acCategory') {
            parent::set('hide_children_in_tree', false);
            // Show children
            $c = $this->xpdo->newQuery('acProduct');
            $c->command('UPDATE');
            $c->where(array(
                'parent' => $this->id,
                'class_key' => 'acProduct',
            ));
            $c->set(array(
                'show_in_tree' => true,
            ));
            $c->prepare();
            $c->stmt->execute();
        }

        return parent::save($cacheFlag);
    }


    public function getNeighborhood()
    {
        $arr = array();

        $c = $this->xpdo->newQuery('acCategory', array('parent' => $this->parent, 'class_key' => 'acCategory'));
        $c->sortby('menuindex', 'ASC');
        $c->select('id');
        if ($c->prepare() && $c->stmt->execute()) {
            $ids = $c->stmt->fetchAll(PDO::FETCH_COLUMN);
            $current = array_search($this->id, $ids);

            $right = $left = array();
            foreach ($ids as $k => $v) {
                if ($k > $current) {
                    $right[] = $v;
                } else {
                    if ($k < $current) {
                        $left[] = $v;
                    }
                }
            }

            $arr = array(
                'left' => array_reverse($left),
                'right' => $right,
            );
        }

        return $arr;
    }


    /**
     * @param array $node
     *
     * @return array
     */
    public function prepareTreeNode(array $node = array())
    {
        $classes = array_map('trim', explode(' ', $node['cls']));
        $remove = array('pnew_modStaticResource', 'pnew_modSymLink', 'pnew_modWebLink', 'pnew_modDocument');
        $node['cls'] = implode(' ', array_diff($classes, $remove));
        $node['hasChildren'] = true;
        $node['expanded'] = false;

        return $node;
    }


    /**
     * @param array $options
     *
     * @return mixed
     */
    public function duplicate(array $options = array())
    {
        $category = parent::duplicate($options);

        $options = $this->getMany('CategoryOptions');
        /** @var msCategoryOption $option */
        foreach ($options as $option) {
            $option->set('category_id', $category->get('id'));

            /** @var msCategoryOption $new */
            $new = $this->xpdo->newObject('msCategoryOption');
            $new->fromArray($option->toArray(), '', true, true);
            $new->save();
        }

        return $category;
    }
}