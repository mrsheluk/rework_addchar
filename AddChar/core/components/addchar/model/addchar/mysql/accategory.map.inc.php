<?php
$xpdo_meta_map['acCategory']= array (
  'package' => 'addchar',
  'version' => '0.2',
  'extends' => 'msCategory',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'class_key' => 'acCategory',
  ),
  'fieldMeta' => 
  array (
    'class_key' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '100',
      'phptype' => 'string',
      'null' => false,
      'default' => 'acCategory',
    ),
  ),
  'composites' => 
  array (
    'OwnProducts' => 
    array (
      'class' => 'acProduct',
      'local' => 'id',
      'foreign' => 'parent',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'AlienProducts' => 
    array (
      'class' => 'msCategoryMember',
      'local' => 'id',
      'foreign' => 'category_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'CategoryOptions' => 
    array (
      'class' => 'msCategoryOption',
      'local' => 'id',
      'foreign' => 'category_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
