miniShop2.combo.Char = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        xtype: 'superboxselect',
        allowBlank: true,
        msgTarget: 'under',
        allowAddNewData: true,
        addNewDataOnBlur: true,
        pinList: false,
        resizable: true,
        name: config.name,
        anchor: '100%',
        minChars: 1,
        store: new Ext.data.JsonStore({
            id: config.name + '-store',
            root: 'results',
            autoLoad: false,
            autoSave: false,
            totalProperty: 'total',
            fields: ['value'],
            url: miniShop2.config['addchar_connector_url'],
            baseParams: {
                action: 'mgr/product/getchar',
                key: config.name
            }
        }),
        mode: 'remote',
        displayField: 'value',
        valueField: 'value',
        triggerAction: 'all',
        extraItemCls: 'x-tag',
        expandBtnCls: 'x-form-trigger',
        clearBtnCls: 'x-form-trigger',
    });
    config.name += '[]';

    Ext.apply(config, {
        listeners: {
            newitem: function(bs, v) {
                bs.addNewItem({value: v});
            },
        },
    });

    miniShop2.combo.Char.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.Char, Ext.ux.form.SuperBoxSelect);
Ext.reg('addchar-combo-char', miniShop2.combo.Char);

miniShop2.combo.CategoryAdditional = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        id: 'minishop2-combo-section',
        fieldLabel: _('ms2_category'),
        description: '<b>[[*parent]]</b><br />' + _('ms2_product_parent_help'),
        fields: ['id', 'pagetitle', 'parents'],
        valueField: 'id',
        displayField: 'pagetitle',
        name: 'parent-cmb',
        hiddenName: 'parent-cmp',
        allowBlank: false,
        url: miniShop2.config['addchar_connector_url'],
        baseParams: {
            action: 'mgr/category/getcats',
            combo: true,
            id: config.value
        },
        tpl: new Ext.XTemplate('\
            <tpl for="."><div class="x-combo-list-item minishop2-category-list-item">\
                <tpl if="parents">\
                    <div class="parents">\
                        <tpl for="parents">\
                            <nobr><small>{pagetitle} / </small></nobr>\
                        </tpl>\
                    </div>\
                </tpl>\
                <span>\
                    <small>({id})</small> <b>{pagetitle}</b>\
                </span>\
            </div></tpl>', {
            compiled: true
        }),
        itemSelector: 'div.minishop2-category-list-item',
        pageSize: 20,
        editable: true
    });
    miniShop2.combo.CategoryAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.CategoryAdditional, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-category-additional', miniShop2.combo.CategoryAdditional);