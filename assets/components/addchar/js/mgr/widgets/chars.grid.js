addchar.grid.Characteristics = function (config) {
    config = config || {};
    if (!config.id) {
        config.id = 'addchar-grid-chars';
    }
    Ext.applyIf(config, {
        url: addchar.config.connector_url,
        fields: this.getFields(config),
        columns: this.getColumns(config),
        tbar: this.getTopBar(config),
        sm: new Ext.grid.CheckboxSelectionModel(),
        baseParams: {
            action: 'mgr/char/getlist'
        },
        listeners: {
            rowDblClick: function (grid, rowIndex, e) {
                var row = grid.store.getAt(rowIndex);
                this.updateChar(grid, e, row);
            }
        },
        paging: true,
        remoteSort: true,
        autoHeight: true
    });
    addchar.grid.Characteristics.superclass.constructor.call(this, config);

    // Clear selection on grid refresh
    this.store.on('load', function () {
        if (this._getSelectedKeys().length) {
            this.getSelectionModel().clearSelections();
        }
    }, this);
};
Ext.extend(addchar.grid.Characteristics, MODx.grid.Grid, {
    windows: {},

    getMenu: function (grid, rowIndex) {
        var keys = this._getSelectedKeys();

        var row = grid.getStore().getAt(rowIndex);
        var menu = addchar.utils.getMenu(row.data['actions'], this, keys);

        this.addContextMenuItem(menu);
    },

    createChar: function (btn, e) {
        var w = MODx.load({
            xtype: 'addchar-char-window-create',
            id: Ext.id(),
            listeners: {
                success: {
                    fn: function () {
                        this.refresh();
                    }, scope: this
                }
            }
        });
        w.reset();
        //w.setValues({active: true});
        w.show(e.target);
    },

    updateChar: function (btn, e, row) {
        if (typeof(row) !== 'undefined') {
            this.menu.record = row.data;
        }
        else if (!this.menu.record) {
            return false;
        }
        var key = this.menu.record.key;

        MODx.Ajax.request({
            url: this.config.url,
            params: {
                action: 'mgr/char/get',
                key: key
            },
            listeners: {
                success: {
                    fn: function (r) {
                        var w = MODx.load({
                            xtype: 'addchar-char-window-update',
                            id: Ext.id(),
                            record: r,
                            listeners: {
                                success: {
                                    fn: function () {
                                        this.refresh();
                                    }, scope: this
                                }
                            }
                        });
                        w.reset();
                        w.setValues(r.object);
                        w.show(e.target);
                    }, scope: this
                }
            }
        });
    },

    removeChar: function () {
        var keys = this._getSelectedKeys();
        if (!keys.length) {
            return false;
        }
        MODx.msg.confirm({
            title: keys.length > 1
                ? _('addchar_grid_chars_delete')
                : _('addchar_grid_char_delete'),
            text: keys.length > 1
                ? _('addchar_grid_chars_delete_confirm')
                : _('addchar_grid_char_delete_confirm'),
            url: this.config.url,
            params: {
                action: 'mgr/char/remove',
                keys: Ext.util.JSON.encode(keys)
            },
            listeners: {
                success: {
                    fn: function () {
                        this.refresh();
                    }, scope: this
                }
            }
        });
        return true;
    },

    getFields: function () {
        return ['key', 'title', 'type','actions'];
    },

    getColumns: function () {
        return [
            {
                header: _('addchar_grid_title_key'),
                dataIndex: 'key',
                sortable: true,
                width: 200
            }, {
                header: _('addchar_grid_title_name'),
                dataIndex: 'title',
                sortable: true,
                width: 250
            }, {
                header: _('addchar_grid_title_type'),
                dataIndex: 'type',
                sortable: false,
                width: 100
            }, {
                header: _('addchar_grid_title_actions'),
                dataIndex: 'actions',
                renderer: addchar.utils.renderActions,
                sortable: false,
                width: 75,
                id: 'actions'
            }];
    },

    getTopBar: function () {
        return [{
            text: '<i class="icon icon-plus"></i>&nbsp;'+_('addchar_grid_button_create'),
            handler: this.createChar,
            scope: this
        }, '->', {
            xtype: 'addchar-field-search',
            width: 250,
            listeners: {
                search: {
                    fn: function (field) {
                        this._doSearch(field);
                    }, scope: this
                },
                clear: {
                    fn: function (field) {
                        field.setValue('');
                        this._clearSearch();
                    }, scope: this
                }
            }
        }];
    },

    onClick: function (e) {
        var elem = e.getTarget();
        if (elem.nodeName === 'BUTTON') {
            var row = this.getSelectionModel().getSelected();
            if (typeof(row) !== 'undefined') {
                var action = elem.getAttribute('action');
                if (action === 'showMenu') {
                    var ri = this.getStore().find('key', row.key);
                    return this._showMenu(this, ri, e);

                } else if (typeof this[action] === 'function') {
                    this.menu.record = row.data;
                    return this[action](this, e);
                }
            }
        }
        return this.processEvent('click', e);
    },

    _getSelectedKeys: function () {
        var keys = [];
        var selected = this.getSelectionModel().getSelections();

        for (var i in selected) {
            if (!selected.hasOwnProperty(i)) {
                continue;
            }
            keys.push(selected[i]['data']['key']);
        }

        return keys;
    },

    _doSearch: function (tf) {
        this.getStore().baseParams.query = tf.getValue();
        this.getBottomToolbar().changePage(1);
    },

    _clearSearch: function () {
        this.getStore().baseParams.query = '';
        this.getBottomToolbar().changePage(1);
    }
});
Ext.reg('addchar-grid-chars', addchar.grid.Characteristics);