miniShop2.page.CreateProductAdditional = function (config) {
    config = config || {record: {}};
    config.record = config.record || {};

    Ext.applyIf(config, {
        panelXType: 'minishop2-panel-product-create-additional',
        mode: 'create'
    });
    miniShop2.page.CreateProductAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.page.CreateProductAdditional, miniShop2.page.CreateProduct);
Ext.reg('minishop2-page-product-create-additional', miniShop2.page.CreateProductAdditional);


miniShop2.panel.CreateProductAdditional = function (config) {
    config = config || {};
    miniShop2.panel.CreateProductAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.panel.CreateProductAdditional, miniShop2.panel.ProductAdditional);
Ext.reg('minishop2-panel-product-create-additional', miniShop2.panel.CreateProductAdditional);