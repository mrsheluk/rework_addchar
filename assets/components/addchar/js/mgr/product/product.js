miniShop2.panel.ProductAdditional = function (config) {
    config = config || {};
    miniShop2.panel.ProductAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.panel.ProductAdditional, miniShop2.panel.Product, {

    getFields: function (config) {
        var fields = [];
        var originals = MODx.panel.Resource.prototype.getFields.call(this, config);

        for (var i in originals) {
            if (!originals.hasOwnProperty(i)) {
                continue;
            }
            var item = originals[i];
            if (item.id == 'modx-resource-header') {
                item.html = '<h2>' + _('ms2_product_new') + '</h2>';
            }
            else if (item.id == 'modx-resource-tabs') {
                item.stateful = MODx.config['ms2_product_remember_tabs'] == 1;
                item.stateId = 'minishop2-product-' + config.mode + '-tabpanel';
                item.stateEvents = ['tabchange'];
                item.collapsible = false;
                item.getState = function () {
                    return {activeTab: this.items.indexOf(this.getActiveTab())};
                };

                var product = [];
                var other = [];

                for (var i2 in item.items) {
                    if (!item.items.hasOwnProperty(i2)) {
                        continue;
                    }
                    var tab = item.items[i2];
                    switch (tab.id) {
                        case 'modx-resource-settings':
                            tab.items.push(this.getContent(config));
                            product.push(tab);
                            break;
                        case 'modx-page-settings':
                            tab.items = this.getProductSettings(config);
                            product.push(tab);
                            if (miniShop2.config['show_extra']) {
                                product.push(this.getProductFields(config));
                            }
                            if (miniShop2.config['addchar_additional'].length) {
                                product.push(this.getProductAdditional(config));
                            }
                            if (miniShop2.config['show_options']) {
                                var options = this.getProductOptions(config);
                                if (options) {
                                    product.push(options);
                                }
                            }
                            if (config.mode == 'update' && miniShop2.config['show_links']) {
                                product.push(this.getProductLinks(config));
                            }
                            if (miniShop2.config['show_categories']) {
                                product.push(this.getProductCategories(config));
                            }
                            break;
                        default:
                            other.push(tab);
                    }
                }

                var tabs = [{
                    title: _('ms2_tab_product'),
                    cls: 'panel-wrapper',
                    id: 'minishop2-product-tab',
                    items: [{
                        xtype: 'modx-tabs',
                        id: 'minishop2-product-tabs',
                        stateful: MODx.config['ms2_product_remember_tabs'] == 1,
                        stateId: 'minishop2-product-' + config.mode + '-tabpanel-product',
                        stateEvents: ['tabchange'],
                        getState: function () {
                            return {activeTab: this.items.indexOf(this.getActiveTab())};
                        },
                        deferredRender: false,
                        items: product,
                        resource: config.resource,
                        border: false,
                        listeners: {},
                    }]
                }];

                item.items = tabs.concat(other);
            }
            if (item.id != 'modx-resource-content') {
                fields.push(item);
            }
        }

        return fields;
    },

    getProductAdditional: function (config) {

        var fields = miniShop2.config.addchar_additional;
        var col1 = [];
        var col2 = [];
        var tmp, length = fields.length;
        var help;

        for (var i = 0; i < length; i++) {
            help = '<br/>' + _('ac_product_char_' + fields[i].key + '_help');
            tmp = {
                fieldLabel : fields[i].title,
                description: '<b>[[*' + fields[i].key + ']]</b>' + help,
                value: config.record[fields[i].key],
                anchor: '100%',
                enableKeyEvents: true,
                listeners: config.listeners,
                name: fields[i].key,
                id: 'modx-resource-addchar-' + fields[i].key,
                msgTarget: 'under',
            };
            switch (fields[i].type) {
                case 'integer':
                    tmp.xtype = 'numberfield';
                    break;
                case 'json':
                    tmp.xtype = 'addchar-combo-char';
                    break;
                case 'float':
                    tmp.xtype = 'numberfield';
                    tmp.decimalPrecision = 2;
                    break;
                default:
                    tmp.xtype = 'textfield';
            }
            if (i % 2) {
                col2.push(tmp);
            }
            else {
                col1.push(tmp);
            }
            Ext.applyIf(fields[i].key, tmp);
        }

        return {
            title: _('ac_product_tab_name'),
            bodyCssClass: 'main-wrapper',
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: .5,
                    layout: 'form',
                    labelAlign: 'top',
                    items: col2,
                }, {
                    columnWidth: .5,
                    layout: 'form',
                    labelAlign: 'top',
                    items: col1,
                }],
            }],
            listeners: {},
        };
    },

    getProductSettings: function (config) {
        var originals = MODx.panel.Resource.prototype.getSettingFields.call(this, config);

        var moved = {};
        var items = [];
        for (var i in originals[0]['items']) {
            if (!originals[0]['items'].hasOwnProperty(i)) {
                continue;
            }
            var column = originals[0]['items'][i];
            var fields = [];
            for (var i2 in column['items']) {
                if (!column['items'].hasOwnProperty(i2)) {
                    continue;
                }
                var field = column['items'][i2];
                switch (field.id) {
                    case 'modx-resource-content-type':
                        field.xtype = 'hidden';
                        field.value = MODx.config['default_content_type'] || 1;
                        break;
                    case 'modx-resource-content-dispo':
                        field.xtype = 'hidden';
                        field.value = config.record['content_dispo'] || 0;
                        break;
                    case 'modx-resource-menuindex':
                        moved.menuindex = field;
                        continue;
                    case 'modx-resource-parent':
                        field.xtype = 'minishop2-combo-category-additional';
                        field.listeners = {
                            select: {
                                fn: function (data) {
                                    Ext.getCmp('modx-resource-parent-hidden').setValue(data.value);
                                }
                            }
                        };
                        break;
                    case undefined:
                        if (field.xtype == 'fieldset') {
                            this.findField(field, 'modx-resource-isfolder', function (f) {
                                f.disabled = true;
                                f.hidden = true;
                            });
                            field.items[0].items[0].items = [
                                this.getExtField(config, 'show_in_tree', {xtype: 'xcheckbox'})
                            ].concat(field.items[0].items[0].items);
                            moved.checkboxes = field;
                            continue;
                        }
                        else {
                            break;
                        }
                }
                fields.push(field);
            }
            column.items = fields;
            items.push(column);
        }
        if (moved.checkboxes != undefined) {
            items[0]['items'].push(moved.checkboxes);
        }
        if (moved.menuindex != undefined) {
            items[1]['items'].push(moved.menuindex);
        }
        originals[0]['items'] = items;

        return originals[0];
    },
});