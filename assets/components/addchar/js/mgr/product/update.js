miniShop2.page.UpdateProductAdditional = function (config) {
    config = config || {record: {}};
    config.record = config.record || {};

    Ext.applyIf(config, {
        panelXType: 'minishop2-panel-product-update-additional',
        mode: 'update'
    });
    miniShop2.page.UpdateProductAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.page.UpdateProductAdditional, miniShop2.page.UpdateProduct);
Ext.reg('minishop2-page-product-update-additional', miniShop2.page.UpdateProductAdditional);


miniShop2.panel.UpdateProductAdditional = function (config) {
    config = config || {};
    miniShop2.panel.UpdateProductAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.panel.UpdateProductAdditional, miniShop2.panel.ProductAdditional, {

    getFields: function (config) {
        var fields = [];
        var originals = miniShop2.panel.ProductAdditional.prototype.getFields.call(this, config);

        for (var i in originals) {
            if (!originals.hasOwnProperty(i)) {
                continue;
            }
            var item = originals[i];
            if (item.id == 'modx-resource-tabs') {
                // Additional tabs
                if (miniShop2.config['show_comments'] != 0) {
                    item.items.push(this.getComments(config));
                }
                if (miniShop2.config['show_gallery'] != 0) {
                    item.items.push(this.getGallery(config));
                }
            }
            fields.push(item);
        }

        return fields;
    },

    getComments: function (config) {
        return {
            title: _('ms2_tab_comments'),
            layout: 'anchor',
            items: [{
                xtype: 'tickets-panel-comments',
                record: config.record,
                parents: config.record.id,
                border: false,
            }]
        };
    },

    getGallery: function (config) {
        return {
            title: _('ms2_tab_product_gallery'),
            layout: 'anchor',
            items: [{
                xtype: 'minishop2-gallery-page-additional',
                record: config.record,
                pageSize: 50,
                border: false,
            }]
        };
    },
});
Ext.reg('minishop2-panel-product-update-additional', miniShop2.panel.UpdateProductAdditional);