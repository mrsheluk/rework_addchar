miniShop2.panel.ImagesAdditional = function (config) {
    config = config || {};

    this.view = MODx.load({
        xtype: 'minishop2-gallery-images-view-additional',
        id: 'minishop2-gallery-images-view-additional'
    });

    miniShop2.panel.ImagesAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.panel.ImagesAdditional, miniShop2.panel.Images);
Ext.reg('minishop2-gallery-images-panel-additional', miniShop2.panel.ImagesAdditional);

miniShop2.view.ImagesAdditional = function (config) {
    config = config || {};

    Ext.applyIf(config, {
        url: miniShop2.config.addchar_connector_url
    });

    miniShop2.view.ImagesAdditional.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.view.ImagesAdditional, miniShop2.view.Images);
Ext.reg('minishop2-gallery-images-view-additional', miniShop2.view.Images);