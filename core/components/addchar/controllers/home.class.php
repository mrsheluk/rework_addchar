<?php

/**
 * The home manager controller for addchar.
 *
 */
class addcharHomeManagerController extends modExtraManagerController
{
    /** @var addchar $addchar */
    public $addchar;


    /**
     *
     */
    public function initialize()
    {
        $this->addchar = $this->modx->getService('addchar', 'addchar', MODX_CORE_PATH . 'components/addchar/model/');
        parent::initialize();
    }


    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return ['addchar:default'];
    }


    /**
     * @return bool
     */
    public function checkPermissions()
    {
        return true;
    }


    /**
     * @return null|string
     */
    public function getPageTitle()
    {
        //return $this->modx->lexicon('addchar');
        return 'Дополнительные характеристики';
    }


    /**
     * @return void
     */
    public function loadCustomCssJs()
    {
        $this->addCss($this->addchar->config['cssUrl'] . 'mgr/main.css');
        $this->addJavascript($this->addchar->config['jsUrl'] . 'mgr/addchar.js');
        $this->addJavascript($this->addchar->config['jsUrl'] . 'mgr/misc/utils.js');
        $this->addJavascript($this->addchar->config['jsUrl'] . 'mgr/misc/combo.js');
        $this->addJavascript($this->addchar->config['jsUrl'] . 'mgr/widgets/chars.grid.js');
        $this->addJavascript($this->addchar->config['jsUrl'] . 'mgr/widgets/chars.windows.js');
        $this->addJavascript($this->addchar->config['jsUrl'] . 'mgr/widgets/home.panel.js');
        $this->addJavascript($this->addchar->config['jsUrl'] . 'mgr/sections/home.js');

        $this->addHtml('<script type="text/javascript">
        addchar.config = ' . json_encode($this->addchar->config) . ';
        addchar.config.connector_url = "' . $this->addchar->config['connectorUrl'] . '";
        Ext.onReady(function() {MODx.load({ xtype: "addchar-page-home"});});
        </script>');
    }


    /**
     * @return string
     */
    public function getTemplateFile()
    {
        $this->content .= '<div id="addchar-panel-home-div"></div>';

        return '';
    }
}