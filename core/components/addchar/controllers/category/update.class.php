<?php

require_once MODX_CORE_PATH . 'components/minishop2/controllers/category/update.class.php';

class acCategoryUpdateManagerController extends msCategoryUpdateManagerController
{
    /** @var acCategory $resource */
    public $resource;


    /**
     * Returns language topics
     * @return array
     */
    public function getLanguageTopics()
    {
        return array_merge(['addchar:default'], parent::getLanguageTopics());
    }


    /**
     * Register custom CSS/JS for the page
     */
    public function loadCustomCssJs()
    {
        $mgrUrl = $this->modx->getOption('manager_url', null, MODX_MANAGER_URL);
        $assetsUrl = $this->miniShop2->config['assetsUrl'];
        $this->addchar = $this->modx->getService('addChar', 'addchar', MODX_CORE_PATH . 'components/addchar/model/');
        $jsPath = $this->addchar->config['jsUrl'];

        /** @var msProduct $product */
        $product = $this->modx->newObject('msProduct');
        $product_fields = array_merge(
            $product->getAllFieldsNames(),
            array('actions', 'preview_url', 'cls', 'vendor_name', 'category_name')
        );

        if (!$category_grid_fields = $this->modx->getOption('ms2_category_grid_fields')) {
            $category_grid_fields = 'id,pagetitle,article,price,weight,image';
        }

        $category_grid_fields = array_map('trim', explode(',', $category_grid_fields));
        $grid_fields = array_values(array_intersect($category_grid_fields, $product_fields));
        if (!in_array('actions', $grid_fields)) {
            $grid_fields[] = 'actions';
        }

        if ($this->resource instanceof acCategory) {
            $neighborhood = $this->resource->getNeighborhood();
        }

        $this->addCss($assetsUrl . 'css/mgr/main.css');
        $this->addCss($assetsUrl . 'css/mgr/bootstrap.buttons.css');
        $this->addJavascript($mgrUrl . 'assets/modext/util/datetime.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/element/modx.panel.tv.renders.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/resource/modx.grid.resource.security.local.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/resource/modx.panel.resource.tv.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/resource/modx.panel.resource.js');
        $this->addJavascript($mgrUrl . 'assets/modext/sections/resource/update.js');
        $this->addJavascript($assetsUrl . 'js/mgr/minishop2.js');
        $this->addJavascript($assetsUrl . 'js/mgr/misc/ms2.combo.js');
        $this->addJavascript($assetsUrl . 'js/mgr/misc/ms2.utils.js');
        $this->addJavascript($assetsUrl . 'js/mgr/misc/default.grid.js');
        $this->addJavascript($assetsUrl . 'js/mgr/misc/default.window.js');
        $this->addJavascript($assetsUrl . 'js/mgr/category/category.common.js');
        $this->addJavascript($assetsUrl . 'js/mgr/category/option.grid.js');
        $this->addJavascript($assetsUrl . 'js/mgr/category/option.windows.js');
        $this->addJavascript($assetsUrl . 'js/mgr/category/product.grid.js');
        $this->addJavascript($jsPath . 'mgr/category/product.grid.js');
        $this->addLastJavascript($assetsUrl . 'js/mgr/category/update.js');
        $this->addLastJavascript($jsPath . 'mgr/category/update.js');

        $showComments = (int)(class_exists('TicketsSection') && $this->modx->getOption('ms2_category_show_comments'));
        $config = array(
            'assets_url' => $this->miniShop2->config['assetsUrl'],
            'connector_url' => $this->miniShop2->config['connectorUrl'],
            'show_comments' => $showComments,
            'product_fields' => $product_fields,
            'grid_fields' => $grid_fields,
            'default_thumb' => $this->miniShop2->config['defaultThumb'],
            'addchar_connector_url' => $this->addchar->config['connectorUrl'],
        );
        $ready = array(
            'xtype' => 'minishop2-page-category-update-additional',
            'resource' => $this->resource->get('id'),
            'record' => $this->resourceArray,
            'publish_document' => $this->canPublish,
            'preview_url' => $this->previewUrl,
            'locked' => $this->locked,
            'lockedText' => $this->lockedText,
            'canSave' => $this->modx->hasPermission('mscategory_save'),
            'canEdit' => $this->canEdit,
            'canCreate' => $this->canCreate,
            'canDuplicate' => $this->canDuplicate,
            'canDelete' => $this->canDelete,
            'canPublish' => $this->canPublish,
            'show_tvs' => !empty($this->tvCounts),
            'next_page' => !empty($neighborhood['right'][0]) ? $neighborhood['right'][0] : 0,
            'prev_page' => !empty($neighborhood['left'][0]) ? $neighborhood['left'][0] : 0,
            'up_page' => $this->resource->parent,
            'mode' => 'update',
        );

        $this->addHtml('
        <script type="text/javascript">
        // <![CDATA[
            MODx.config.publish_document = "' . $this->canPublish . '";
            MODx.onDocFormRender = "' . $this->onDocFormRender . '";
            MODx.ctx = "' . $this->ctx . '";
            miniShop2.config = ' . json_encode($config) . ';
            Ext.onReady(function() {
                MODx.load(' . json_encode($ready) . ');
            });
        // ]]>
        </script>');

        // load RTE
        $this->loadRichTextEditor();
        $this->modx->invokeEvent('msOnManagerCustomCssJs', array('controller' => &$this, 'page' => 'category_update'));
        $this->loadPlugins();

        // Load Tickets
        if ($showComments) {
            $this->loadTickets();
        }
    }
}
