<?php

$_lang['addchar_prop_limit'] = 'Ограничение вывода Предметов на странице.';
$_lang['addchar_prop_outputSeparator'] = 'Разделитель вывода строк.';
$_lang['addchar_prop_sortby'] = 'Поле сортировки.';
$_lang['addchar_prop_sortdir'] = 'Направление сортировки.';
$_lang['addchar_prop_tpl'] = 'Чанк оформления каждого ряда Предметов.';
$_lang['addchar_prop_toPlaceholder'] = 'Усли указан этот параметр, то результат будет сохранен в плейсхолдер, вместо прямого вывода на странице.';
