<?php

class acProductGetProcessor extends modObjectGetProcessor
{
    public $classKey = 'acProduct';
    public $languageTopics = array('minishop2:default');

}

return 'acProductGetProcessor';