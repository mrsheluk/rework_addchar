<?php

class acProductShowInTreeProcessor extends modObjectUpdateProcessor
{
    public $classKey = 'acProduct';


    /**
     * @return bool
     */
    public function beforeSet()
    {
        $this->properties = array(
            'show_in_tree' => true
        );

        return true;
    }

}

return 'acProductShowInTreeProcessor';