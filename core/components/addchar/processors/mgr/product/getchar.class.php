<?php

class acProductGetCharProcessor extends modObjectProcessor
{
    public $classKey = 'acProductChar';


    public function process()
    {
        $query = trim($this->getProperty('query'));
        $limit = trim($this->getProperty('limit', 10));
        $key = preg_replace('#^options-(.*?)#', '$1', $this->getProperty('key'));

        $c = $this->modx->newQuery($this->classKey);
        $c->query['distinct'] = 'DISTINCT';
        $c->sortby('value', 'ASC');
        $c->select('value');
        $c->groupby('value');
        $c->where(array('key_char' => $key));
        $c->limit($limit);
        if (!empty($query)) {
            $c->where(['value:LIKE' => "%{$query}%"]);
            $c->where(['value:LIKE' => '%' . $this->toJSON($query) . '%'],xPDOQuery::SQL_OR);
        }
        $found = false;
        $output = [];
        if ($c->prepare() && $c->stmt->execute()) {
            while ($res = $c->stmt->fetch(PDO::FETCH_ASSOC)) {
                $res = json_decode($res['value'], 1);
                foreach ($res as $item) {
                    if (stripos($item, $query) !== false && !in_array(['value' => $item], $output)) $output[] = ['value' => $item];
                    if ($item == $query) $found = true;
                }
            }
        }

        if (!$found && !empty($query)) {
            $output = array_merge_recursive(array(array('value' => $query)), $output);
        }

        return $this->outputArray($output);
    }

}

return 'acProductGetCharProcessor';