<?php

class acProductMultipleProcessor extends modProcessor
{


    /**
     * @return array|string
     */
    public function process()
    {
        if (!$method = $this->getProperty('method', false)) {
            return $this->failure();
        }
        $ids = json_decode($this->getProperty('ids'), true);
        if (empty($ids)) {
            return $this->success();
        }

        /** @var miniShop2 $miniShop2 */
        $addchar = $this->modx->getService('addchar', 'addchar', MODX_CORE_PATH . 'components/addchar/model/');

        foreach ($ids as $id) {
            /** @var modProcessorResponse $response */
            $response = $addchar->runProcessor('mgr/product/' . $method, array('id' => $id));
            if ($response->isError()) {
                return $response->getResponse();
            }
        }

        return $this->success();
    }

}

return 'acProductMultipleProcessor';