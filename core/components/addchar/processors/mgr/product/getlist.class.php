<?php

require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/product/getlist.class.php';

class acProductGetListProcessor extends msProductGetListProcessor
{
    public $classKey = 'acProduct';


    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $c->where(array('class_key' => 'acProduct'));
        $c->leftJoin('msProductData', 'Data', 'acProduct.id = Data.id');
        $c->leftJoin('msCategoryMember', 'Member', 'acProduct.id = Member.product_id');
        $c->leftJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
        $c->leftJoin('acCategory', 'Category', 'Category.id = acProduct.parent');
        if ($this->getProperty('combo')) {
            $c->select('acProduct.id,acProduct.pagetitle,acProduct.context_key');
        } else {
            $c->select($this->modx->getSelectColumns('acProduct', 'acProduct'));
            $c->select($this->modx->getSelectColumns('msProductData', 'Data', '', array('id'), true));
            $c->select($this->modx->getSelectColumns('msVendor', 'Vendor', 'vendor_', array('name')));
            $c->select($this->modx->getSelectColumns('acCategory', 'Category', 'category_', array('pagetitle')));
        }
        if ($this->item_id) {
            $c->where(array('acProduct.id' => $this->item_id));
            if ($parent = (int)$this->getProperty('parent')) {
                $this->parent = $parent;
            }
        } else {
            $query = trim($this->getProperty('query'));
            if (!empty($query)) {
                if (is_numeric($query)) {
                    $c->where(array(
                        'acProduct.id' => $query,
                        'OR:Data.article:=' => $query,
                    ));
                } else {
                    $c->where(array(
                        'acProduct.pagetitle:LIKE' => "%{$query}%",
                        'OR:acProduct.longtitle:LIKE' => "%{$query}%",
                        'OR:acProduct.description:LIKE' => "%{$query}%",
                        'OR:acProduct.introtext:LIKE' => "%{$query}%",
                        'OR:Data.article:LIKE' => "%{$query}%",
                        'OR:Data.made_in:LIKE' => "%{$query}%",
                        'OR:Vendor.name:LIKE' => "%{$query}%",
                        'OR:Category.pagetitle:LIKE' => "%{$query}%",
                    ));
                }
            }

            $parent = (int)$this->getProperty('parent');
            if (!empty($parent)) {
                $category = $this->modx->getObject('modResource', $this->getProperty('parent'));
                $this->parent = $parent;
                $parents = array($parent);

                $nested = $this->getProperty('nested', null);
                $nested = $nested === null && $this->modx->getOption('ms2_category_show_nested_products', null, true)
                    ? true
                    : (bool)$nested;
                if ($nested) {
                    $tmp = $this->modx->getChildIds($parent, 10, array('context' => $category->get('context_key')));
                    foreach ($tmp as $v) {
                        $parents[] = $v;
                    }
                }
                $c->orCondition(array('parent:IN' => $parents, 'Member.category_id:IN' => $parents), '', 1);
            }
        }

        return $c;
    }
}

return 'acProductGetListProcessor';