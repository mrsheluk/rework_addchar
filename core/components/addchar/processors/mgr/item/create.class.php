<?php

class addcharItemCreateProcessor extends modObjectCreateProcessor
{
    public $objectType = 'addcharItem';
    public $classKey = 'addcharItem';
    public $languageTopics = ['addchar'];
    //public $permission = 'create';


    /**
     * @return bool
     */
    public function beforeSet()
    {
        $name = trim($this->getProperty('name'));
        if (empty($name)) {
            $this->modx->error->addField('name', $this->modx->lexicon('addchar_item_err_name'));
        } elseif ($this->modx->getCount($this->classKey, ['name' => $name])) {
            $this->modx->error->addField('name', $this->modx->lexicon('addchar_item_err_ae'));
        }

        return parent::beforeSet();
    }

}

return 'addcharItemCreateProcessor';