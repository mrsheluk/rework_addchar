<?php

class addcharItemRemoveProcessor extends modObjectProcessor
{
    public $objectType = 'addcharItem';
    public $classKey = 'addcharItem';
    public $languageTopics = ['addchar'];
    //public $permission = 'remove';


    /**
     * @return array|string
     */
    public function process()
    {
        if (!$this->checkPermissions()) {
            return $this->failure($this->modx->lexicon('access_denied'));
        }

        $ids = $this->modx->fromJSON($this->getProperty('ids'));
        if (empty($ids)) {
            return $this->failure($this->modx->lexicon('addchar_item_err_ns'));
        }

        foreach ($ids as $id) {
            /** @var addcharItem $object */
            if (!$object = $this->modx->getObject($this->classKey, $id)) {
                return $this->failure($this->modx->lexicon('addchar_item_err_nf'));
            }

            $object->remove();
        }

        return $this->success();
    }

}

return 'addcharItemRemoveProcessor';