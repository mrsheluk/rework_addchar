<?php

require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/category/getlist.class.php';

class acCategoryGetListProcessor extends msCategoryGetListProcessor
{
    public $classKey = 'acCategory';


    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $c->where(array(
            'class_key' => 'acCategory',
        ));

        if ($query = $this->getProperty('query')) {
            $c->where(array('pagetitle:LIKE' => "%$query%"));
        }

        return $c;
    }
}

return 'acCategoryGetListProcessor';