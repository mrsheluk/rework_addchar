<?php

require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/category/getnodes.class.php';

class acCategoryGetNodesProcessor extends msCategoryGetNodesProcessor
{
    /**
     * @param modResource $resource
     *
     * @return array
     */
    public function prepareResourceNode(modResource $resource)
    {
        $qtipField = $this->getProperty('qtipField');
        $nodeField = $this->getProperty('nodeField');
        $nodeFieldFallback = $this->getProperty('nodeFieldFallback');

        $hasChildren = (int)$resource->get('childrenCount') > 0 && $resource->get('hide_children_in_tree') == 0;

        // Assign an icon class based on the class_key
        $class = $iconCls = array();
        $classKey = strtolower($resource->get('class_key'));
        if (substr($classKey, 0, 3) == 'mod') {
            $classKey = substr($classKey, 3);
        }

        $classKeyIcon = $this->modx->getOption('mgr_tree_icon_' . $classKey, null, 'tree-resource');
        $iconCls[] = $classKeyIcon;

        $class[] = 'icon-' . strtolower(str_replace('mod', '', $resource->get('class_key')));
        if (!$resource->get('isfolder')) {
            $class[] = 'x-tree-node-leaf icon-resource';
        }
        if (!$resource->get('published')) {
            $class[] = 'unpublished';
        }
        if ($resource->get('deleted')) {
            $class[] = 'deleted';
        }
        if ($resource->get('hidemenu')) {
            $class[] = 'hidemenu';
        }
        if ($hasChildren) {
            $class[] = 'haschildren';
            if ($resource->get('class_key') != 'acCategory') {
                $iconCls[] = $this->modx->getOption('mgr_tree_icon_folder', null, 'tree-folder');
            }
            $iconCls[] = 'parent-resource';
        }

        $qtip = '';
        if (!empty($qtipField)) {
            $qtip = '<b>' . strip_tags($resource->$qtipField) . '</b>';
        } else {
            if ($resource->longtitle != '') {
                $qtip = '<b>' . strip_tags($resource->longtitle) . '</b><br />';
            }
            if ($resource->description != '') {
                $qtip = '<i>' . strip_tags($resource->description) . '</i>';
            }
        }

        $idNote = $this->modx->hasPermission('tree_show_resource_ids')
            ? ' <span dir="ltr">(' . $resource->id . ')</span>'
            : '';

        if (!$text = strip_tags($resource->$nodeField)) {
            $text = strip_tags($resource->$nodeFieldFallback);
        }
        $itemArray = array(
            'text' => $text . $idNote,
            'id' => $resource->context_key . '_' . $resource->id,
            'pk' => $resource->id,
            'cls' => implode(' ', $class),
            'iconCls' => implode(' ', $iconCls),
            'type' => 'modResource',
            'classKey' => $resource->class_key,
            'ctx' => $resource->context_key,
            'hide_children_in_tree' => $resource->hide_children_in_tree,
            'qtip' => $qtip,
            'checked' => !empty($resource->member) || $resource->id == $this->parent_id,
            'disabled' => $resource->id == $this->parent_id,
        );
        if (!$hasChildren) {
            $itemArray['hasChildren'] = false;
            $itemArray['children'] = array();
            $itemArray['expanded'] = true;
        } else {
            $itemArray['hasChildren'] = true;
        }

        if ($itemArray['classKey'] != 'acCategory') {
            unset($itemArray['checked']);
        }

        return $itemArray;
    }

}

return 'acCategoryGetNodesProcessor';