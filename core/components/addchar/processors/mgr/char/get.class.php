<?php

class acCharGetProcessor extends modObjectGetProcessor {

    public $classKey = 'acChar';
    public $primaryKeyField = 'key';
    public $languageTopics = ['default', 'addchar:default'];

}

return 'acCharGetProcessor';