<?php

class acCharUpdateProcessor extends modObjectUpdateProcessor {

    public $classKey = 'acChar';
    public $primaryKeyField = 'key';
    public $languageTopics = ['default', 'addchar:default'];

}

return 'acCharUpdateProcessor';