<?php

class acCharGetListProcessor extends modObjectGetListProcessor {

    public $classKey = 'acChar';
    public $primaryKeyField = 'key';
    public $languageTopics = ['default', 'addchar:default'];
    public $defaultSortField = 'key';

    public function prepareQueryBeforeCount(xPDOQuery $c) {
        $query = trim($this->getProperty('query'));
        if ($query) {
            $c->where([
                'key:LIKE' => "%$query%",
                'OR:title:LIKE' => "%$query%"
            ]);
        }
        return $c;
    }

    public function prepareRow(xPDOObject $object) {
        $object_array = $object->toArray();
        $names = [
            'integer' => 'Число',
            'float' => 'С плавающей точкой',
            'string' => 'Строка',
            'json' => 'JSON'
        ];
        $object_array['type'] = $names[$object_array['type']];
        $object_array['actions'] = [];

        // button "edit" in grid action column
        $object_array['actions'][] = [
            'cls' => '',
            'icon' => 'icon icon-edit',
            'title' => $this->modx->lexicon('addchar_common_edit'),
            //'multiple' => $this->modx->lexicon('modextra_items_update'),
            'action' => 'updateChar',
            'button' => true,
            'menu' => true,
        ];

        // button "delete" in grid action column
        $object_array['actions'][] = [
            'cls' => '',
            'icon' => 'icon icon-trash-o action-red',
            'title' => $this->modx->lexicon('addchar_common_delete'),
            'multiple' => $this->modx->lexicon('addchar_common_delete'),
            'action' => 'removeChar',
            'button' => true,
            'menu' => true,
        ];

        return $object_array;
    }

}

return 'acCharGetListProcessor';